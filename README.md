# clearoutsidebot

Simple mastodon bot to respond to clearoutside toots with current weather predictions from https://www.clearoutside.com.
Open https://botsin.space/@clearoutside to see the bot in action. Send a toot in the form of @clearoutside@botsin.space (12.2,22.2) to get a response.

# Installation
```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

# Run
Simply call the bot.py which will catch the latest notifications and send responses

