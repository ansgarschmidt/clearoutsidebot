import re
import os
import requests
from   mastodon import Mastodon

def getCoordinates(post):
    pos_coords = re.findall(r'(\( *-?[0-9]+\.[0-9]+ *, *-?[0-9]+\.[0-9]+ *\))', post['content'])

    if len(pos_coords) == 0:
        return None

    coord = None

    for i in pos_coords:
        c = i.replace(" ","")[1:-1].split(",")
        lat = float(c[0])
        lon = float(c[1])

        if lat <= -90 or lat >= 90:
            continue

        if lon <= -180 or lon >= 180:
            continue

        coord = f"{lat:.2f}/{lon:.2f}"
    return coord

def getImageAndUpload(coord, name, mastodon):
    media = None

    try:
        url = f"https://clearoutside.com/forecast_image_large/{coord}/forecast.png"
        r   = requests.get(url)
        open("/tmp/forecast.png", "wb").write(r.content)
        media = mastodon.media_post("/tmp/forecast.png", description=f"Weather prediction for astronomical observations for {coord}")

        if os.path.exists("/tmp/forecast.png"):
            os.remove("/tmp/forecast.png")
    except:
        print("error with downloads")

    return media, f"Moin @{name}, here are the current weather predictions in great details #bot #servicetoot: https://clearoutside.com/forecast/{coord}"

with open("/root/clearoutsidebot/lastid.txt", "r") as f:
    min_id = int(f.readline())

mastodon = Mastodon(access_token = '/root/clearoutsidebot/token.secret', api_base_url = 'https://botsin.space')
toots    = mastodon.notifications(types = ["mention"], min_id = min_id)

for toot in toots:
    min_id = toot['id']
    name   = toot['account']['acct']
    userid = toot['account']['id']
    id     = toot['status']['id']
    status = toot['status']
    mastodon.status_favourite(id)
    mastodon.account_follow(userid)
    coord  = getCoordinates(status)
    print(f"Send message to {name}")

    if coord == None:
        text = f"Moin @{name}, please use \"(lat,long)\" e.g. (52.61,13.6) in your post to get current weather predictions. #bot #servicetoot"
        mastodon.status_post(text, in_reply_to_id = id)
    else:
        media, text = getImageAndUpload(coord, name, mastodon)
        mastodon.status_post(text, in_reply_to_id = id, media_ids = [media])

with open("/root/clearoutsidebot/lastid.txt", "w") as f:
    f.write(f"{min_id}")
